/* global require Headlines */
require(['dojo/_base/kernel', 'dojo/ready'], function  (dojo, ready) {
	ready(function () {
		Headlines.default_move_on_expand = false;
	});
});

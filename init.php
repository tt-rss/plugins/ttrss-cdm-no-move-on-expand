<?php
class CDM_No_Move_On_Expand extends Plugin {
	function about() {
		return array(null,
			"Prevents article top from moving when clicking in unexpanded combined mode (whew!)",
			"fox");
	}

	function init($host) {

	}

	function get_js() {
		return file_get_contents(__DIR__ . "/init.js");
	}

	function api_version() {
		return 2;
	}
}
